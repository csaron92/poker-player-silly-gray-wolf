﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace Nancy.Simple
{
    public static class PokerPlayer
    {
        public static readonly string VERSION = "GetToTheChoppa2";

        public static int BetRequest(JObject gameState)
        {
            RootObject rootObject = gameState.ToObject<RootObject>(); // start
            var selfCards = rootObject.players[rootObject.in_action].hole_cards; //start2
            var stack = rootObject.players[rootObject.in_action].stack;
            var smallBlind = rootObject.small_blind;

            var hand = new Hand { cards = selfCards };

            //rebuild asdssdasds nem megy a gitlab :'(
            if (isHighPair(hand))
                return 777777777;
            else
                return 0;

            //all-in, ha nagyon kevés lóvénk van
            if (isStackLowerThanBigBlind(stack, smallBlind))
                return 500000000;

            //6-osnál nagyobb pár asdasda
            if (isHighPair(hand))
            {
                //flopon allin, előtte csak megad
                if (rootObject.community_cards.Count > 0)
                    return 5000000;
                else
                    return rootObject.current_buy_in;
            }



            if (hand.isAK_suited())
            {
                //flopon all-in, ha találtunk párt, egyébként dobás
                if (rootObject.community_cards.Count > 0)
                {
                    if (pairOnFlop(selfCards, rootObject.community_cards))
                        return 500000;
                    else
                        return 0;
                }
                else
                {
                    //flop előtt csak megadunk
                    return rootObject.current_buy_in;
                }
            }



            //TODO: Use this method to return the value You want to bet
            return 0;
        }

        public static void ShowDown(JObject gameState)
        {
            //TODO: Use this method to showdown
        }

        public static bool isStackLowerThanBigBlind(int stack, int smallBlind)
        {
            if (stack <= 2 * smallBlind)
                return true;
            return false;
        }

        public static bool isHighPair(Hand hand)
        {

            return hand.isPocketPair() && hand.cards[0].isHigherThan(10);
        }

        public static bool isSuitedConnectedCards(List<HoleCard> selfCards)
        {
            int firstCard = mapCardToInt(selfCards[0]);
            int secondCard = mapCardToInt(selfCards[1]);

            if (selfCards[0].suit == selfCards[1].suit && (firstCard > 7 || secondCard > 7))
            {
                if ((firstCard - secondCard) * (firstCard - secondCard) == 1)
                {
                    return true;
                }
            }

            return false;
        }

        public static int mapCardToInt(HoleCard card)
        {
            switch (card.rank)
            {
                case "2":
                    return 2;
                case "3":
                    return 3;
                case "4":
                    return 4;
                case "5":
                    return 5;
                case "6":
                    return 6;
                case "7":
                    return 7;
                case "8":
                    return 8;
                case "9":
                    return 9;
                case "10":
                    return 10;
                case "J":
                    return 11;
                case "Q":
                    return 12;
                case "K":
                    return 13;
                case "A":
                    return 14;
                default:
                    return 0;
            }
        }

        public static bool pairOnFlop(List<HoleCard> selfCards, List<HoleCard> communityCards)
        {
            foreach (var communityCard in communityCards)
            {

                if (communityCard.rank == selfCards[0].rank ||
                    communityCard.rank == selfCards[1].rank)
                    return true;
            }
            return false;
        }
    }

    public class Hand
    {
        public List<HoleCard> cards { get; set; }

        /// <summary>
        /// AKs (or any specific suited cards)	0.00302	330.5 : 1
        /// A és K azonos szín
        /// </summary>  
        /// <returns></returns>
        public bool isAK_suited() { return isSuited() && cards.Any(x => x.rank.Equals("A")) && cards.Any(y => y.rank.Equals("K")); }
        /// <summary>
        /// Egy pár 16:1
        /// </summary>
        /// <returns></returns>
        public bool isPocketPair() { return cards.Select(x => x.rank).Distinct().Count() == 1; }

        /// <summary>
        /// Azonos színű kártyák 3,25:1
        /// </summary>
        /// <returns></returns>
        public bool isSuited() { return cards.Select(x => x.suit).Distinct().Count() == 1; }


        /// <summary>
        /// AA (or any specific pair)	0.00452	220 : 1
        /// </summary>
        /// <returns></returns>
        public bool isAA() { return cards.Select(x => x.rank.Equals("A")).Count() == 2; }

        /// <summary>
        /// Azonos színŰ AK, KQ, QJ, J10 	81.9 : 1
        /// </summary>
        public bool isAK_KQ_QJ_JTen_suited()
        {
            return isSuited() && ((hasCard("A") && hasCard("K") || (hasCard("K") && hasCard("Q"))
                || (hasCard("J") && hasCard("Q")) || (hasCard("J") && hasCard("10"))));
        }

        /// <summary>
        /// AA, KK, or QQ	0.0136	72.7 : 1
        /// </summary>
        /// <returns></returns>
        public bool isAA_KK_QQ() { return isPocketPair() && (hasCard("A") || hasCard("K") || hasCard("Q")); }

        public bool hasCard(string rank) { return cards.Select(x => x.rank.Equals(rank)).Count() >= 1; }

    }

    public class HoleCard
    {
        public string rank { get; set; }
        public string suit { get; set; }
    }

    public class Player
    {
        public int id { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public string version { get; set; }
        public int stack { get; set; }
        public int bet { get; set; }
        public List<HoleCard> hole_cards { get; set; }
    }

    public class RootObject
    {
        public string tournament_id { get; set; }
        public string game_id { get; set; }
        public int round { get; set; }
        public int bet_index { get; set; }
        public int small_blind { get; set; }
        public int current_buy_in { get; set; }
        public int pot { get; set; }
        public int minimum_raise { get; set; }
        public int dealer { get; set; }
        public int orbits { get; set; }
        public int in_action { get; set; }
        public List<Player> players { get; set; }
        public List<HoleCard> community_cards { get; set; }
    }

    public static class CardHelper
    {
        public static int getRankInt(this HoleCard card)
        {
            return PokerPlayer.mapCardToInt(card);
        }

        public static bool isHigherThan(this HoleCard card, int r)
        {
            return card.getRankInt() > r;
        }
    }
}